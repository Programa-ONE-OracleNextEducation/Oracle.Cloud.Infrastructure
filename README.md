# **Oracle Cloud Infrastructure**

Cloud computing o computación en la nube, posibilita el acceso a sistemas, archivos y procesamiento de datos por internet. Esta tecnologia está en constante crecimiento y la búsqueda por profesionales preparados para trabajar con cloud viene expandiéndose también.

La formación de Oracle Cloud Infrastructure va a ayudarte a crear tus primeros servicios en la nube de Oracle.

Esta formación forma parte del Programa ONE, una alianza entre Alura Latam y Oracle.

## **De quien vas a aprender**

- Harland Lohora

## **Paso a paso**

### **1. Conociendo OCI**

En esta etapa vas a aprender los conceptos básicos de computación en nube y compreenderás la arquitectura de Oracle Cloud. Vas a crear tus primeros servicios en la nube de Oracle como ser redes virtuales y un load balancer. Vas a aprender a crear y configurar un compute para hospedar una aplicación.

[Oracle Cloud Infrastructure: implementación de una aplicación en la nube](https://app.aluracursos.com/course/oracle-cloud-infrastructure-implementacion-aplicacion-nube "Oracle Cloud Infrastructure: implementación de una aplicación en la nube"). Contenido [aqui](./Docs/oracle_cloud_infrastructure_implementacion_aplicacion_nube.md).

[Oracle Cloud Infrastructure: base de datos e infraestructura como código](https://app.aluracursos.com/course/oracle-cloud-infrastructure-base-datos-infraestructura-codigo "Oracle Cloud Infrastructure: base de datos e infraestructura como código"). Contenido [aqui](./Docs/oracle_cloud_infrastructure_base_datos_infraestructura_codigo.md).
