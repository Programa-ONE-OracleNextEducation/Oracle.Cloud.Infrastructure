# **Oracle Cloud Infrastructure - Base de datos e infraestructura como código**

## **Base de datos en OCI**

### **Eligiendo una base de datos**

Durante el desarrollo de la arquitectura de una aplicación bancaria, un equipo trajo la necesidad de elegir una alternativa de base de datos para almacenar los datos de sus clientes. De las alternativas disponibles en OCI, Autonomous Database permite las organizaciones a desarrollar e implementar cargas de trabajo independiente de la complejidad, escalabilidad o criticidad.

Selecciona las alternativas que presentan características del Autonomous Database:

Rta.

- Automatización de detección, failover y reparo de fallas. Autonomous Database detecta y protege contra fallas del sistema y errores de los usuarios automáticamente y provide failover para base de datos con cero pérdida de datos.
- Automatización de aprovisionamiento, ajuste y dimensionamiento de base de datos. Autonomous Database provide bases de datos, configuraciones, ajustes de alta disponibilidad y dimensiona recursos de computación cuando necesario.
- Automatización de la protección y seguridad de datos. Autonomous Database protege datos confidenciales y reglamentados automáticamente, además de corregir la base de datos cuando a vulnerabilidades de seguridad e impide el acceso no autorizado.

### **Para saber más: base de datos Oracle**

Oracle desarrolla una de las bases de datos más utilizadas en el medio corporativo del mundo. De esta forma, es de esperarse que Oracle Cloud dispone muchos tipos de recursos de bases de datos adecuados a las diversas necesidades de sus clientes. Este video detalla las características técnicas de cada una de las opciones de bases de datos de la Oracle Cloud y es recomendable asistir si ya eres un administrador de bases de datos o si tienes contacto más profundo con el área.

[Database Level 100 - Part 1 - Introduction to OCI Database Service](https://youtu.be/F4-sxIsnbKI "Base de Datos")

El contenido en video puede presentar algunos tópicos desactualizados, por eso, todos los detalles sobre la utilización de Base de Datos en OCI pueden ser encontrados en la documentación oficial en la sección “Gerenciamento de Datos” a través del link:

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/home.htm "Documentation")

### **Para saber más: base de datos No Relacionales**

Además de las soluciones tradicionales de bases de datos relacionales, también está disponible en OCI la Oracle Database API para MongoDB.

Con esta API, los desarrolladores pueden utilizar las herramientas y drivers de código abierto de MongoDB conectados a un Oracle Autonomous JSON Database manteniendo los beneficios de una base de datos autónoma juntamente con una API de base de datos No Relacionales.

Como la API es compatible con Mongo DB, frecuentemente, poca o ninguna alteración es necesaria en el código de las aplicaciones existentes, basta alterar la string de conexión.

La lista de lenguajes de programación recomendadas incluye C, C#, Go, Java, Node.js, Ruby, entre otras.

El paso a paso de como utilizar Oracle Database API para MongoDB encontrase disponible en este [link](https://docs.oracle.com/en/cloud/paas/autonomous-database/adbsa/mongo-using-oracle-database-api-mongodb.html "Using Oracle Database API for MongoDB").

---

## **Conectando una API y la base de datos**

### **Haga lo que hicimos: ejecutando la API de Doguito en OCI**

Aprendimos cómo implementar una aplicación Node.JS en la infraestructura de OCI. Clonamos el repositorio de git de la aplicación en la instancia, en seguida instalamos Node.JS, drivers de base de datos y por fin liberamos firewall y la regla de entrada para que la aplicación fuera accesible.

Llegó la hora de que practiques lo que aprendiste y ejecutar la Doguito API en OCI. Para tener acceso a los códigos, puedes acceder al link del repositorio en [Github](https://github.com/HarlandLohora/doguito-api-es "Repository").

### **Haga lo que hicimos: probando la API REST de Doguito**

Probamos la API REST de Doguito utilizando un plugin de navegador. Existen varias opciones de plugins de navegadores, pero utilizamos Boomerang, que está disponible en los enlaces abajo:

- [Chrome](https://chrome.google.com/webstore/detail/boomerang-soap-rest-clien/eipdnjedkpcnlmmdfdkgfpljanehloah?hl=pt-BR "Boomerang - SOAP & REST Client")
- [Edge](https://microsoftedge.microsoft.com/addons/detail/boomerang-soap-rest-c/bhmdjpobkcdcompmlhiigoidknlgghfo "Boomerang - SOAP & REST Client")

Si utilizas Firefox la única opción es [RESTClient](https://addons.mozilla.org/pt-BR/firefox/addon/restclient/ "RESTClient, a debugger for RESTful web services")

Pero vimos que la ejecución de Doguito es realizada de forma manual, lo que no es ideal una vez que si la instancia sea reiniciada o ocurrir algún problema, Doguito no será ejecutado otra vez. Para evitar este problema, convertimos Doguito en un servicio de Systemd.

Ahora es contigo, haga los ajustes recomendados y pruebe Doguito API con un cliente REST. Si tienes alguna dificultad, usa el fóro para conversar con otras personas sobre tus dudas.

### **Para saber más: tecnologías del Doguito**

Nuestra aplicación de ejemplo que será implementada en Oracle Cloud es desarrollada utilizando JavaScript, NodeJS y el framework Express. Para nuestro curso no será necesario conocimiento profundo de desarrollo de aplicaciones utilizando estas tecnologías, pues vamos a enfocarnos en la infraestructura de la implementación.

### **Permitiendo acceso a la aplicación**

Imagina que implementamos un aplicación en OCI, pero no podemos acceder por internet aún sabiendo que está en ejecución en la instancia. Su IP simplemente no contesta y también no aparece ningún mensaje de error que informa lo que pasó. La página solo se queda cargando.

Elige las opciones abajo con situaciones que pueden tener ese síntoma.

Rta.

- Falta de una regla de entrada en el listado de seguridad de VCN. Para que una VCN reciba comunicación de redes externas es necesario habilitar una regla de entrada para la puerta donde se desea recibir la comunicación.
- Falta de configuración del firewall de la instancia. Por patrón diversas puertas de comunicación son bloqueadas en el firewall de la instancia, para que haya comunicación es necesario habilitar estas puertas en el firewall.

---

## **Almacenamiento**

### **Haga lo que hicimos: página estática del Doguito**

Aprendimos a crear un bucket público de un Object Storage que nos permitió servir la página estática de Doguito, o sea, archivos HTML, CSS y JavaScript de manera muy simple y práctica.

Llegó tu vez de hacer lo mismo. Para eso, será necesario hacer la descarga del [archivo del curso](https://github.com/alura-es-cursos/1911-OCI2-doguito-site/archive/refs/heads/master.zip "folder.zip") “doguito-site-object-storage.zip” en zip, crear un bucket y hacer el upload de los archivos.

### **Haga lo que hicimos: Doguito API con SSL**

Vimos que para que nuestro front end pueda comunicarse con la API es necesario que ambos utilicen protocolos HTTPS. De esta manera, necesitamos de un medio para acceder la API de Doguito via HTTPS.

La forma más común de hacerlo es un certificado HTTPS en el balanceador de cargas, entonces es necesario crear un certificado, importarlo en el balanceador y en seguida incluir un listener para la puesta 443 en el balanceador. Por fin, hacemos ajustes para que las requisiciones del balanceador sean direccionadas para back end en la puerta 3000.

Ajustado ese primer paso, en seguida debemos alterar el archivo script.js, que pasará a apuntar para el IP público del balanceador de cargas y utilizar el protocolo HTTPS. Ahora llegó tu vez de hacer la activación.

### **Tipos de almacenamiento adecuado**

OCI ofrece diversos tipos de almacenamiento para cada caso de uso y necesidad. Cuando creamos una instancia esta está asociada a un volumen de inicialización que posee la imagen del sistema operacional para realizar el boot. En el caso de que esa instancia necesite expandir su almacenamiento o caso deseamos mantener archivos que serán preservados aunque la instancia sea destruida, debemos utilizar un tipo de almacenamiento específico para eso.

Selecciona la alternativa que corresponde ao armazenamento mais adequado para este caso.

Rta.

Volumen en blocos. El servicio de volumen de bloques permite provisional y gerencias dinámicamente volúmenes de almacenamiento en bloque como si fueran una unidad de disco rígido común. También puedes desconectar un volumen y adjuntarlo a otra instancia sin pérdida de datos. Esta es la opción más adecuada para expandir la capacidad de almacenamiento de una instancia.

### **Para saber más: tecnologías del Doguito**

Object Storage es un concepto nuevo que se ha popularizado en diversos proveedores de Cloud. Esto se debe a su facilidad de creación y baja necesidad de manutención. Sus usos pueden variar desde la utilización para upload de contenido de usuarios hasta persistencia de configuración de las aplicaciones. Para saber un poco más de Object Storagm, asiste a este video de Oracle que explica muchas características suyas:

[Introduction to Oracle Cloud Infrastructure Object Storage](https://youtu.be/IePCpBGza0k "Object Storage")

El contenido en video puede presentar algunos tópicos desactualizados, por eso, todos los detalles sobre la utilización de Almacenamiento en OCI pueden ser encontrados en la documentación oficial en la sección “Almacenamiento” a través del link:

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/home.htm "Documentation")

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Que la OCI ofrece diversos tipos de almacenamientos para tu aplicación, como el almacenamiento en bloques, almacenamiento de archivos y el Object Storage. Cada categoría presenta diversas características de performance y durabilidad, debiendo ser evaluadas las necesidades específicas de cada caso de uso.
- Que Object Storage permite acceso HTTPS directo a los recursos, a partir de la definición de un namespace y de un bucket.
- Que podemos utilizar Object Storage para almacenar y servir el front end de Doguito Petshop, siendo una forma simplificada para servir una página vía HTTPS.
- Que cuando el front end es servido en HTTPS es necesario que el backend también sea servido en HTTPS para que el navegador acepte realizar requisiciones AJAX entre los dominios.

---

## **Infraestructura como código**

### **Haga lo que hicimos: infraestructura a partir del código**

Realizamos la creación de nuestra primera infraestructura a partir del código utilizando el gerenciador de recursos de OCI.

Inicialmente fue necesario remover los recursos ya utilizados para no ultrapasar los límites de uso de la cuenta free tier.

Realizamos la remoción del balanceador de cargas, de las dos instancias de computación y de la red virtual.

En seguida pudimos implementar nuestra primera infraestructura como código como el archivo “orm-dps.zip”, que tiene una infraestructura básica de balanceador de carga, red virtual y dos instancias.

Sigue los pasos realizados en el video para implementar la infraestructura como código y si tienes problemas, acciona el fóro del curso para obtener auxilio.

### **Para saber más: automatizando la infraestructura**

La infraestructura necesaria para la ejecución de nuestras aplicaciones se han tornado cada vez más complejas. Además de eso, también tenemos el concepto de infraestructura elástica, que consiste en adecuar nuestra infraestructura implementada al volumen de trabajo de un determinado momento. Para eso, surgió la necesidad de automatización de implementación, que debe dejar de ser un proceso manual para ser un proceso automático y reproducible.

Como hemos visto, Resource Manager de OCI es nuestro aliado en esta tarea, y este video nos trae muchas informaciones complementares para profundizarnos nuestro conocimiento sobre Resource Manager:

[Resource Manager](https://youtu.be/btnRgK36LnE "Resource Manager")

El contenido en video puede presentar algunos tópicos desactualizados, por eso, todos los detalles sobre la utilización de Resource Manager en OCI pueden ser encontrados en la documentación oficial a través del link:

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/home.htm "Documentation")

### **Ventajas de IAC**

Infraestructura como código es un abordaje muy adoptado actualmente, en especial cuando se utilizan prácticas de DevOps en las organizaciones. Eso porque hay ventajas reales en esta forma de gerenciamiento y aprovisionamiento en comparación a la implementación manual de infraestructuras.

Marca las alternativas con las opciones que corresponden a los beneficios de IAC.

Rta.

- Reproductibilidad de la infraestructura. El código que representa una infraestructura puede ser ejecutado un número indefinido de veces tornando la infraestructura fácilmente reproducible.
- Control de versión de la infraestructura. Con la infraestructura como código, el código que representa la infraestructura puede ser versionado utilizando las herramientas convencionales de versionamiento como Git.
- Automatización del aprovisionamiento. La principal ventaja de utilizar infraestructura como código es la automatización del aprovisionamiento, eliminando prácticamente todo el proceso manual y pasible de error de implementación de infraestructura.

### **Lo que aprendimos**

- Que las aplicaciones han evolucionado su arquitectura de construcción de monolitos para microservicios.
- Que la utilización de microservicios acelera la necesidad de trabajarse con automatización de aprovisionamiento de infraestructura y infraestructura como código.
- Que la OCI disponibiliza diversas soluciones de soporte al desarrollo a través de la opción “Servicios al Desarrollador”, por ejemplo: Kubernets, Registro de Artefactos, Gateway de API y Gerenciamiento de Recursos (IaaS)
- Que el Gerenciador de Recursos es un servicio de Terraform y permite la definición de infraestructura como código, colaborando para que la infraestructura pueda ser fácilmente replicada y versionada.

---

## **Infraestructura del Doguito como código**

### **Haga lo que hicimos: Doguito como código**

¿Estás listo para crear un pilla customizada para implementar Doguito de manera extremadamente simple y práctica?

Serán necesarios algunos pasos:

- Ajustar el archivo “compute.tf” customizando el nombre de las instancias;
- Ajustar el archivo loadbalancer.tf customizando el nombre del balanceador de cargas y las puertas de conexión;
- Ajustar el archivo “network.tf” incluyendo una regla de ingreso para la puerta 3000;
- Crear un nuevo bucket llamado internal y hacer el upload de los archivos “doguito-site.service” y “Wallet_DOGUITODB.zip”;
- Alterar el “archivo cloud-init.yaml” instalando git, Node.js y los drivers de conexión con la base de datos ORacle. Inserir un comando en el archivo para hacer la descarga y configurar el service de Doguito, además de hacer la descarga de Wallet y la descompactar en el local adecuado, liberando la puerta 3000 en firewall.

Por fin, tenemos que compactar otra vez el archivo “orm-dps-vm2” en un archivo zip que será utilizado para inicializar la pilla del Doguito.

Si tienes alguna pregunta, no dudes en entrar en contacto con otras personas en nuestro fóro.

### **Haga lo que hicimos: implementando Doguito como código**

Tenemos el archivo “orm-dps-v2.zip” y ahora es hora de implementarlo como una pilla en OCI. Para eso utilizamos la opción de Servicios al Desarrollador y Gerenciador de Recursos.

El proceso será muy semejante al que hicimos anteriormente, pero utilizaremos el archivo “orm-dps-v2.zip” que acabamos de generar para subir una pila customizada para Doguito.

Espero que salga todo bien y que tu pilla sea implementada con éxito, pero si tienes algún problema, puedes conversar con los demás colegas en el foro.

### **Para saber más: automatizando la infraestructura**

En este entrenamiento nuestro enfoque es en el aprendizaje de utilización de Oracle Cloud para casos prácticos, entretanto, debes haber visto que hablamos sobre varias herramientas auxiliares que podemos utilizar para trabajar en una Cloud.

Resource Manager de OCI es una de estas herramientas y que internamente utiliza scripts TerraForm para automatizar la implementación de infraestructura.

Si te interesaste por Terraform y quieres saber más sobre, te dejamos un artículo que tenemos en nuestro blog [aquí](https://www.aluracursos.com/blog/conociendo-terraform?utm_source=gnarus&utm_medium=timeline "Alura LATAM").

### **Implementando recursos como código**

Para facilitar la implementación del Doguito y permitir que esta implementación sea versionada y fácilmente evolucionada, utilizamos la funcionalidad de OCI llamada gerenciador de recursos. Nos posibilita crear una nueva pilla que puede definir determinados tipos de recursos computacionales.

Selecciona las alternativas que corresponden a estos recursos.

- Gateways. Los gateways pueden ser configurados como parte de una pilla en el archivo network.tf.
- Balanceadores de carga. Los balanceadores de carga pueden ser configurados como parte de una pilla en el archivo loadbalancer.tf.
- Instancias de computación. Las instancias de computación pueden ser configuradas como parte de una pilla en el archivo compute.tf.

### **Lo que aprendimos**

- Que podemos customizar los archivos Terraform para crear diversos tipos de recursos computacionales de una pilla de plementación.
- Que utilizamos el archivo cloud-init.yaml para customizar la instalación de softwares diversos en las instancias computacionales creadas a partir de una pilla.
- Que usamos el Almacenamiento de Objetos para guardar archivos de configuración que serán utilizados en la implementación de nuestras pillas de recursos.
