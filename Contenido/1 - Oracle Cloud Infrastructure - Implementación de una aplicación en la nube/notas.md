# **Oracle Cloud Infrastructure - Implementación de una aplicación en la nube**

## **Computación en la nube**

### **¿Que es la computacion en la nube?**

El uso de servicios como servidores, almacenamiento, redes, bases de datos y otros recursos computacionales a través de internet.

**Beneficios del uso de la nube**

- Costo
- Velocidad
- Escalabilidad
- Productividad
- Desempeño
- Confiabilidad
- Seguridad

### **Tipos de nube**

- Nube publica
- Nube privada
- Nube hibrida

### **Tipos de servicios en nube**

- IaaS (Infraestructura como servicio): es aquel en el que nosotros somos responsables de juntar todas las piezas para crear la infraestructura.
- PaaS: tiene un nivel de abstraccioin, en el que ya hay soluciones desarrolladas.
- SaaS (Software como un servicio): en aquel en que nosotros vamos a poder acceder a un servicio desde una pagina web.
- Serverless: vamos a poder definir funciones que se van a disparar a traves de un evento.

### **¿Por qué utilizamos la nube?**

La empresa donde trabajamos está evaluando si debe o no hacer uso de la nube para la implementación de nuevas aplicaciones. Para contribuir en la discusión, realizaste una lista sobre las ventajas del uso de la nube en comparación con tener un data center propio.

Selecciona las alternativas correctas sobre esas ventajas.

Rta.

- Mayor productividad. La computación en la nube nos ayuda a no tener que encargarnos de la manutención de un data center por nosotros mismo, le delegamos esas responsabilidad a nuestro proveedor de nube. Así los equipos de TI pueden enfocarse en otros objetivos de negocio más importantes.
- Mayor confiabilidad. La computación en la nube facilita y reduce el costo de realizar respaldos de nuestros datos, recuperación mediante desastres y la continuidad de los negocios debido a que los datos pueden ser almacenados en diversos sitios redundantes en la red del proveedor de nube.
- Mejor escalabilidad. La computación en la nube permite la escalabilidad elástica de recursos.

### **Para saber más: conceptos de Cloud**

Cuando comenzamos a estudiar sobre el concepto de la nube nos encontramos con una serie de conceptos y siglas que al principio son extraños, sin embargo es importante comprenderlos para poder tener una visión amplia de cómo la nube se compone así como de sus principios. Para ayudarte en este camino tenemos un [artículo](https://www.aluracursos.com/blog/que-es-cloud-y-sus-principales-servicios "Alura LATAM") que resume muy bien estos conceptos relacionados con la nube.

---

## **Cuenta Free Tier en OCI**

### **Creación de cuenta en OCI**

1. Ir a la siguiente [pagina](https://www.oracle.com/cloud/ "Oracle Cloud").
2. Clickear **Try OCI for free** luego a **Start for free**.
3. Registrarse

### **Para saber más: modo gratuito Free Tier**

Si te interesa conocer todos los detalles sobre los límites del usa de la cuenta Free Tier, te recomendamos que vayas al siguiente [enlace](https://www.oracle.com/mx/cloud/free/ "Info") donde encontrarás más información sobre el modo gratuito de Oracle Cloud, encontrarás una explicación más detallada sobre lo que puedes realizar con una cuenta Free Tier así como con una Free Trial.

### **Modo gratuito (Free Tier)**

El Modo Gratuito que nos proporciona Oracle Cloud nos permite utilizar diversos servicios por tiempo ilimitado. Durante nuestro curso solo haremos uso de servicios que se encuentran dentro de esta categoría, por lo cual puedes estar tranquila o tranquilo, ya que no se te hará ningún cobro. Sin embargo una cuenta Free Tier posee algunas características y peculiaridades.

Indica las alternativas correctas sobre una cuenta Modo Gratuito en OCI.

Rta.

- Permite la implementación de servicios de computación y de almacenamiento. Una cuenta Free Tier incluye dos instancias de computación, almacenamiento en bloque con tecnología SSD, almacenamiento de Objetos, almacenamiento de archivos y balanceador de carga
- Es adecuada para la información de aplicaciones en su fase de prueba. Una cuenta Free Tier es adecuada para probar nuevas aplicaciones, en especial para empresas que están evaluando el uso de los servicios de OCI.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Podemos crear una cuenta Free Tier en OCI.
- Existen comandos en la consola de OCI que nos permitirán hacer uso de los servicios(computación, almacenamiento, redes, bases de datos) de OCI para poder implementar nuestra plataforma Doguito Petshop generando.
- Con la cuenta Free Tier podemos utilizar diversos recursos por tiempo ilimitado para poder conocerlos y probarlos sin ningún costo.
- La cuenta Free Tier es diferente del periodo Free Trial. Durante este periodo, podemos utilizar diversos recursos que no están incluidos en la cuenta Free Tier por 30 días o hasta llegar al uso de $300.

---

## **Arquitectura del OCI**

### **Para saber más: infraestructura OCI**

Oracle Cloud es un sistema con diversos conceptos comunes a otras nubes pero también algunos conceptos únicos. Para entender como la infraestructura de Oracle Cloud está dividida asiste el siguiente video. (El video fue realizado en el idioma inglés pero puedes activar los subtítulos en español)

[Getting Started with Oracle Cloud Infrastructure](https://youtu.be/JBkT44FSf0o "You Tube")

El contenido del video puede tener algunos temas desactualizados, por eso todos los detalles sobre los conceptos básicos y la arquitectura de OCI pueden ser encontrados en la documentación oficial en el siguiente enlace.

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/GSG/Concepts/baremetalintro.htm "Documentation")

### **Para saber más: compartimientos a fondo**

En nuestra clase anterior vimos los conceptos básicos de los compartimientos, sin embargo existen detalles técnicos avanzados que son útiles en casos de uso más complejos. El siguiente video da información más detallada (El video fue realizado en el idioma inglés pero puedes activar los subtítulos en español)

[Compartments](https://youtu.be/VJD19vyu6lI "You Tube")

El contenido del video puede tener algunos temas desactualizados, por eso todos los detalles sobre la utilización de compartimientos dentro de OCI pueden ser encontrados en la documentación oficial en el siguiente enlace.

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/Identity/compartments/managingcompartments.htm "Documentation")

### **Haga lo que hicimos: creando compartimentos**

Aprendimos cómo crear compartimentos, que tiene por objetivo promover el isolamiento de seguridad y control de acceso dentro de la nube. Esto se logra a través de un namespace lógico global, donde las políticas de seguridad pueden ser aplicadas. Por medio de la aplicación de políticas, podemos definir un nivel de acceso adecuado dependiendo de los parámetros definidos por la organización para el gerenciamiento de recursos.

De esta manera la creación de compartimentos será nuestra primera dentro de OCI antes de dar continuidad al proceso de implementación de nuestro proyecto.

### **Para saber más: IAM en OCI**

Cuando hablamos de aplicaciones de empresas, el control de acceso a los recursos es esencial para que no ocurran incidentes como fallas de seguridad dentro de nuestra aplicación, Para eso, OCI implementa un modelo avanzado de identidad y control de acceso, si quieres aprender más detalles sobre IAM asiste el siguiente video (El video fue realizado en el idioma inglés pero puedes activar los subtítulos en español).

[Introduction to OCI IAM Service](https://youtu.be/8qaQuoJZYvQ "You Tube")

El contenido del video puede tener algunos temas desactualizados, por eso todos los detalles sobre el concepto de IAM dentro de OCI pueden ser encontrados en la documentación oficial en el siguiente enlace.

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/Identity/iam/manage-iam.htm "Documentation")

### **Haga lo que hicimos: creando usuarios**

Vimos cómo crear nuevos usuarios dentro de Oracle Cloud para agregar el nivel de acceso a las diferentes funcionalidades de OCI.

Al crear tu cuenta por primera vez tendrás acceso a un usuario con permisos de administrador raíz (root). Eso significa que tienes el control sobre toda la cuenta y sobre todos los recursos de OCI. Sin embargo, este usuario debe ser utilizado para gerenciar otros usuarios, grupos y políticas. Debemos crear un nuevo usuario que tenga permisos solamente para el compartimento que creamos anteriormente.

### **Haga lo que hicimos: configurando Cloud Shell**

En esta clase vimos que para que un usuario pueda acceder a Cloud Shell es necesario que tenga las políticas definidas adecuadamente. Para eso tenemos que acceder al área de Identidad y Seguridad de OCI y crear una política para permitir que usuarios del grupo puedan utilizar Cloud Shell.

En seguida debemos acceder a Cloud Shell como un usuario administrador para poder realizar la creación de las llaves SSH que utilizaremos para acceder a las instancias que crearemos después.

### **Compartimentos**

Los compartimentos son recursos importantes dentro de Oracle Cloud y providen un namespace lógico global donde políticas pueden ser implementadas, como carpetas en un sistema de archivos. Por ser globales, se extienden a todas las regiones OCI.

Por ejemplo, los compartments pueden reflejar una estructura funcional de la organización, donde cada departamento tiene un compartment con un administrador designado. Cada compartment de departamento, puede tener subcompartments para diferentes ambientes (Dev, Test, Prod), cada uno con sus propios administradores.

El principal objetivo de los compartments es permitir:

Rta.

El control de acceso y seguridad. Los compartments permiten el control de acceso y seguridad proviniendo un namespace lógico global donde las políticas pueden ser aplicadas, como carpetas en un sistema de archivos.

---

## **Redes**

### **Haga lo que hicimos: VCN para instancias**

Antes de crearnos nuestras instancias de compute, que serán los servidores de Doguito Petshop, tenemos que crear una red virtual en la nube (VCN) donde el compute será prendido.

Una VCN es una red definida por software que se configura en los data centers de Oracle Cloud Infrastructure en determinada región. Esta red puede contener subredes, que son una subdivisión de una VCN.

Para eso debemos acceder el menú “Red”, “Redes Virtuales en la Nube” y empezar el asistente de creación de redes que nos ayudará con esa tarea.

### **Para saber mais: redes en OCI**

Cuando hablamos de infraestructura en Cloud, una de nuestras primeras tareas es estructurar una red. En general estas redes son conocidas como redes virtuales ya que son definidas via software al invés de dispositivos físicos de hardware. Estas redes virtuales poseen diversos conceptos elaborados que puedes profundizar viendo los videos de [esta playlist](https://www.youtube.com/playlist?list=PLvlciYga5j3z7biGjV7-fywS-xEJ3W6Pp "Playlist").

El contenido del video puede presentar algunos tópicos desactualizados, por eso, todos los detalles sobre la utilización de Redes en OCI pueden ser encontrados en la documentación oficial, a través del link:

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/Network/Concepts/landing.htm#top "Documentation")

### **Una VCN**

Una VCN es una red definida por software que configuramos en determinada región de los data centers del Oracle Cloud Infrastructure. Una VCN está dividida en subredes públicas y privadas. Necesitamos definir un servicio para que una instancia de compute en una subrede pueda acceder a la internet, pero no sea accesible a partir de internet.

Selecciona la opción correspondiente al servicio que debemos utilizar.

Rta.

NAT Gateway permite que recursos de la nube sin localización IP públicas accedan a internet sin exponer estos recursos a conexiones de entrada en internet.

---

## **Compute**

### **Haga lo que hicimos: nuestro primer compute**

Creamos nuestro primer compute. Computes son la estructura de la OCI para provisionar y gerenciar hosts de computación, conocidos como instancias.

Puedes crear instancias conforme sea necesario para atender a los aplicativos que deseas implementar en la OCI. Después de crear una instancia, puedes accederla con seguridad utilizando una llave SSH, reiniciarla, adjuntar y desadjuntar volúmenes y cerrarlas. Pero recuerda que cualquier alteración hecha en las unidades locales de la instancia son pérdidas cuando las cierra.

Para crearnos una instancia, que será un servidor de Doguito Petshop, podemos acceder al menú “Computación” y después “Instancias”.

### **Para saber más: todo sobre computes**

Como ya aprendimos, computes representan las instancias de servirán nuestras aplicaciones en cloud. Existen muchas opciones para elegir, que equilibran cuestiones de costo y performance.

Si te interesaste por este tema y quieres saber todos los detalles de la configuración de computes en OCI, tienes que ver [esta playlist](https://www.youtube.com/playlist?list=PLKCk3OyNwIzsAjIaUaVsKdXcfBOy6LASv "Playlist") que nos explica todo sobre computes en OCI.

El contenido del video puede presentar algunos tópicos desactualizados, por eso, todos los detalles sobre la utilización de Redes en OCI pueden ser encontrados en la documentación oficial, a través del link:

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/home.htm "Documentation")

### **Haga lo que hicimos: instalando softwares**

Realizamos la instalación de los softwares necesarios para la ejecución del Doguito Server en la instancia creada. Para eso, instalamos el servidor Apache HTTP y creamos una página HTML muy básica, solamente para validar el funcionamiento del servidor.

Entretanto, para que la instancia sea accesible a través del puerto 80 via internet, es necesario la inclusión de una nueva regla de acceso en la lista de seguridad de la VCN pública.

Después de estos pasos es posible acceder al servidor a través de una requisición via navegador, utilizando el IP Público como localización.

¡Llegó la hora de realizar estos pasos, accede al Cloud Shell y empezamos!

### **Multipla elección Compute**

Cuando realizamos la creación de un Compute en OCI, podemos customizar el Shape (forma del Compute) de acuerdo con las necesidades del sistema que será implementado.

Selecciona las alternativas que indican opciones de customización de Shapes de Compute en OCI:

Rta.

- Tipo de Instancia: Máquina Virtual o Máquina Bare Metal. La máquina virtual es un ambiente computacional independiente que es ejecutado sobre un hardware bare metal físico. Ya una instancia de computación bare metal permite acceso al servidor físico dedicado para fines de desempeño más alto y de mayor aislamiento.
- Cantidad de Memoria (GB). Al crear un compute, la cantidad de memoria RAM definida para el COmpute irá variar de acuerdo con la cantidad de OCPUs que fue definida (informaciones detalladas es este link).
- Cantidad de OCPUs. Al crear un compute se puede definir la cantidad de CPUs virtuales que serán disponibilizadas.

### **Haga lo que hicimos: instalando softwares**

Nuestras expectativas en relación al proyecto de Doguito Petshop son grandes. Entretanto, tenemos que evitar que la página caiga en su lanzamiento, debido a un número muy grande de accesos. Vamos a mantener más de un servidor funcionando y utilizar un balanceador de carga para distribuir la carga entre ellos.

De esta manera, nuestro próximo paso será crear una instancia del Doguito Petshop Server para después implementar un balanceador de cargas.

La creación de este servidor seguirá exactamente los mismos pasos de la primera vez. Entretanto, vamos a alterar su nombre (que parará a ser “dps-vm2”) y el texto de la página HTML patrón, que debe informar que accederemos un segundo servidor.

### **Para saber mais: introducción a load balancers**

Cuando pensamos en aplicaciones escalables horizontalmente, implantadas en cloud y en infraestructuras convencionales, el primer componente que nos viene en mente es un balanceador de cargas. Este servirá para distribuir las cargas del trabajo entre diferentes instancias de servidores de acuerdo con un algoritmo predeterminado.

Puedes encontrar más detalles sobre load balancers en OCI viendo a este video de Oracle:

[Load Balancer](https://youtu.be/tY2UuVbDElc "You Tube")

El contenido del video puede presentar algunos tópicos desactualizados, por eso, todos los detalles sobre la utilización de Redes en OCI pueden ser encontrados en la documentación oficial, a través del link:

[Documentación de Oracle Cloud Infrastructure](https://docs.oracle.com/es-ww/iaas/Content/home.htm "Documentation")

### **Balanceador de cargas**

Cuando creamos un balanceador de carga, puede que exista la necesidad de comunicación de un usuario de la aplicación ocurrir siempre con la misma instancia de back-end. En este caso, podemos definir un algoritmo.

Selecciona la alternativa con algoritmo correcto a ser utilizado en esta situación.

Rta.

IP Hash. Esta política distribuye el tráfico de entrada secuencialmente para cada servidor en una lista de conjuntos de backend.
