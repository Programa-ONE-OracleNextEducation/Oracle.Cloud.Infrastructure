## **Realice este curso para Cloud Computing y:**

- Aprende los conceptos basicos de computación en la nube
- Comprende la arquitectura de Oracle Cloud
- Crea una cuenta en Oracle Cloud
- Aprende a crear redes virtuales u load balancer en Oracle Cloud
- Aprende a crear redes virtuales u load balancer en Oracle Cloud
- Aprende a crear y configurar computes en VCN
- Implementa una aplicación basica en Oracle Cloud

### **Aulas**

- **Computación en la nube**

  - Presentación
  - Doguito Petshop
  - Computación en la nube
  - ¿Por qué utilizamos la nube?
  - Haga lo que hicimos
  - Para saber más: conceptos de Cloud
  - Lo que aprendimos

- **Cuenta Free Tier en OCI**

  - Creación de cuenta en OCI
  - Para saber más: modo gratuito Free TIer
  - Modo gratuito (Free Tier)
  - Haga lo que hicimos: creando cuenta en OCI
  - Conociendo el Console de OCI
  - Haga lo que hicimos: navegando en el Console de OCI
  - Caracteristicas del Free Tier
  - Quien debe usar Free Tier
  - Lo que aprendimos

- **Arquitectura del OCI**

  - Visión general del OCI
  - Para saber más: infraestructura OCI
  - Compartimentos
  - Para saber más: compartimientos a fondo
  - Haga lo que hicimos: creando compartimentos
  - Usuarios
  - Para saber más: IAM en OCI
  - Haga lo que hicimos: creando usuarios
  - Cloud Shell
  - Haga lo que hicimos: configurando Cloud Shell
  - Compartimentos
  - Lo que aprendimos

- **Redes**

  - Redes
  - Creando VCN
  - Haga lo que hicimos: VCN para instancias
  - Rutas y seguridad
  - Para saber mais: redes en OCI
  - Una VCN
  - Lo que aprendimos

- **Compute**

  - Creando una instancia
  - Haga lo que hicimos: nuestro primer compute
  - Para saber más: todo sobre computes
  - Configurando un servidor
  - Haga lo que hicimos: instalando softwares
  - Multipla elección Compute
  - Load Balancer
  - Segunda instancia
  - Haga lo que hicimos: instalando softwares
  - Nuestro Load Balancer
  - Para saber mais: introducción a load balancers
  - Balanceador de cargas
  - Lo que aprendimos
  - Conclusión
  - Créditos
