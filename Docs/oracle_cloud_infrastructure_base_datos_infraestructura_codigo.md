## **Realice este curso para Cloud Computing y:**

- Comprende los conceptos de bases de datos en la nube
- Aprende a crear y configurar un compute para hospedar una aplicación NodeJS
- Hospeda una página estática en Object Storage de Oracle Cloud
- Realiza la implementación de infraestructuras como código en Oracle Cloud

### **Aulas**

- **Base de datos en OCI**

  - Presentación
  - Base de datos en OCI
  - Eligiendo una base de datos
  - Creando una base autonoma
  - Haga lo que hicimos: prepara una base de datos para Doguito
  - Para saber más: base de datos Oracle
  - Para saber más: base de datos No Relacionales
  - Lo que aprendimos

- **Conectando una API y la base de datos**

  - Conociendo Doguito API
  - Implementando una API en OCI
  - Haga lo que hicimos: ejecutando la API de Doguito en OCI
  - Servicio Doguito API
  - Probando la API
  - Haga lo que hicimos: probando la API REST de Doguito
  - Para saber más: tecnologías del Doguito
  - Permitiendo acceso a la aplicación
  - Lo que aprendimos

- **Almacenamiento**

  - Almacenimiento en OCI
  - Doguito en Object Storage
  - Haga lo que hicimos: página estática del Doguito
  - SSL en Load Balancer
  - Conexion https
  - Haga lo que hicimos: Doguito API con SSL
  - Tipos de almacenamiento adecuado
  - Para saber más: tecnologías del Doguito
  - Lo que aprendimos

- **Infraestructura como código**

  - Infraestructura como código
  - Gestor de recursos
  - Nuestra primera IAC
  - Haga lo que hicimos: infraestructura a partir del código
  - Para saber más: automatizando la infraestructura
  - Ventajas de IAC
  - Lo que aprendimos

- **Infraestructura del Doguito como código**

  - IaC Doguito API
  - Haga lo que hicimos: Doguito como código
  - Doguito API
  - Haga lo que hicimos: implementando Doguito como código
  - Para saber más: automatizando la infraestructura
  - Implementando recursos como código
  - Lo que aprendimos
  - Conclusión
  - Créditos
